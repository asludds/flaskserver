import requests
from flaskserver.globalVariables import *
import flaskserver.helperfunctions
import rauth
from flaskserver.main import oauthHandler



def ngpvanMain():
    print("Starting ngpvan server")
    app.run(host="0.0.0.0", port=5000, process=globalVariables.numberOfProcesses)
    print("Terminating ngpvan server")

if __name__=="__main__":
    ngpvanMain()
