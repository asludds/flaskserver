import requests
from globalVariables import *
import helperfunctions
import rauth
from flask import Flask, url_for, session, request, jsonify, g
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy
from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Float, Boolean, desc
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy.ext.declarative import declarative_base
import geopy.distance as distance
import base64
import random
from werkzeug.security import generate_password_hash, check_password_hash
import os
import time

'''
The purpose of this file is to create an executable python server for redirecting requests from the mobile app into nationbuilders. This is done using the nationbuilder API as well as some of the existing code from the API-server
'''
'''
Something to note is that we are trying to mimic the behavior of the client.py file in terms of endpoints, as a result we will be using the same format here
'''

Base = declarative_base()

os.environ['DEBUG'] = globalVariables.OS_DEBUG
os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = globalVariables.OAUTHLIB_INSECURE_TRANSPORT

engine = create_engine(globalVariables.nationBuilderDatabaseURI, echo=True)
Session = sessionmaker(bind=engine)
session = Session()

app = Flask(__name__)
app.debug = globalVariables.debugBoolean
app.config['SQLALCHEMY_DATABASE_URI'] = globalVariables.nationBuilderDatabaseURI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = globalVariables.SQLALCHEMY_TRACK_MODIFICATIONS




class User(Base):
    __tablename__ = "users"
    id = Column(Integer,primary_key=True,unique=True)
    first_name = Column(String)
    last_name = Column(String)
    email = Column(String)
    encrypted_password = Column(String)
    remember_token = Column(String)
    facebook_id = Column(String)
    facebook_access_token = Column(String)
    photo_file_name = Column(String)
    photo_content_type = Column(String)
    photo_file_size = Column(Integer)
    photo_updated_at = Column(Integer)
    base_64_photo_data = Column(String) #This represents the address for the saved photo data
    total_points = Column(Integer)
    state_code = Column(String)
    latitude = Column(Float)
    longitude = Column(Float)

#This below line creates the User database
Base.metadata.create_all(bind=engine)

def oauthVerification(request):
    print("Entering oauthVerification")
    try:
        authorization = request.headers['Authorization']

    except:
        return jsonify({"message": "Unauthenticated"}), 401
    #check to see if already authorized
    if(authorization[0:6]=="Bearer"):
        return True
    decodedClientID, decodedClientSecret =helperfunctions.base64ClientIDClientSecretDecoder(authorization=authorization.split(" ")[1])

    if(str(globalVariables.CLIENT_ID)==decodedClientID and str(globalVariables.CLIENT_SECRET)==decodedClientSecret):
        return True
    else:
        return False



@app.before_request
def before_request():
    g.request_start_time = time.time()
    g.request_time = lambda: "%.5fs" % (time.time() - g.request_start_time)

def printRequestTime():
    print("Request was completed in " + g.request_time())

'''
This endpoint returns oauth tokens
'''
@app.route('/oauth/token',methods=['POST'])
def oauthHandler():
    print("Entering oauthHandler")
    returnedOAuthVerification = oauthVerification(request=request)
    if(returnedOAuthVerification):
        username = request.form['username'] #Note that the username is the email attribute
        password = request.form['password']
        userInDatabase = session.query(User).filter(User.email==str(username)).first()
        passwordInDatabase = userInDatabase.encrypted_password
        passwordCheckBoolean = check_password_hash(pwhash=passwordInDatabase,password=str(password))
        if(passwordCheckBoolean==True):
            access_token = random.randrange(globalVariables.upperBoundRandRange)
            user = session.query(User).filter(User.email==str(username)).update({'remember_token':access_token})
            session.commit()
            json = jsonify(access_token = access_token,expires_in = globalVariables.oauthTimeLimitSeconds, token_type = "bearer", user_id = 1, created_at = 0)
            printRequestTime()
            return json
        else:
            print("Password received by oauthHandler was not correct")
            return 401
    else:
        print("Authentication has failed.")
        return 401


'''
The users post endpoint deals with the creation of new users
'''
@app.route("/users",methods=["POST"])
def usersHandlerNationBuilder():
   print("Entering user post handler")
   json = request.get_json()
   data = json['data']['attributes']
   hashed_password = generate_password_hash(data['password'], method = 'sha512')
   userid = random.randrange(globalVariables.civiCRMIDUpperBound)
   #Check that userid is not already taken
   while True:
       useridquery = session.query(User).filter(User.id==userid).first()
       if(useridquery==None):
           break
       else:
           print("Generating another random number")
           userid = random.randrange(globalVariables.civiCRMIDUpperBound)
   state_code = helperfunctions.findStateGivenLatLng(latitude=data["lat"],longitude=data["lng"])
   if(data["email"]!=None):
       first_name = data['first_name']
       last_name = data['last_name']
       email = data['email']
       home_state = "NA"
       total_points = 0
       state_code = str(state_code)
       visits_count = 0
       latitude = data['lat']
       longitude = data['lng']
       #This is the case where someone signs up with emails
       newuser = User(id=userid,first_name=first_name,last_name=last_name,email=email, encrypted_password = hashed_password, remember_token = "None", facebook_id = "None",
           facebook_access_token = "None", photo_file_name = "None",photo_content_type = "None", photo_file_size = 0,
                      photo_updated_at = 0, base_64_photo_data = "/photos/"+str(userid),total_points=0)
       #note, this is where  in the standard version we would add a user to civiCRM, we don't do that though
       print("Note that newuser has been created")
   elif(data["facebook_access_token"] != None and data["facebook_id"]!=None):
       #This is the case where someone signs up with facebook
       pass
   else:
       print("Request did not have attributes email, facebook_access_token, or facebook_id")
   print("Before user creation")
   session.add(newuser)
   print('After user creation')
   session.commit()
   attributes = helperfunctions.userHandlerPOSTAttributes(email=data["email"],password=data["password"],first_name=data["first_name"],last_name=data["last_name"],
       state_code=str(state_code),latitude=data["lat"],longitude=data["lng"])
   relationships = helperfunctions.userHandlerPOSTRelationships()
   data = helperfunctions.userHandlerPOSTData(userid = userid,attributes = attributes,relationships = relationships)
   printRequestTime()
   return jsonify(data = data)


def userMePatchTryExceptParams(data,email):
     dictionary = {}
     try:
        password = data['password']
        dictionary["password"] = password
        user2 = session.query(User).filter(User.email==str(email)).update({'password':password})
     except:
          password = session.query(User).filter(User.email==str(email)).first().encrypted_password
          dictionary['password'] = password
          pass
     try:
        first_name = data['first_name']
        dictionary["first_name"] = first_name
        user3 = session.query(User).filter(User.email==str(email)).update({'first_name':first_name})
     except:
        first_name = session.query(User).filter(User.email==str(email)).first().first_name
        dictionary["first_name"] = first_name
        pass
     try:
        last_name = data['last_name']
        dictionary["last_name"] = last_name
        user4 = session.query(User).filter(User.email==str(email)).update({'last_name':last_name})
     except:
        last_name = session.query(User).filter(User.email==str(email)).first().last_name
        dictionary["last_name"] = last_name
        pass
     try:
        state_code = data['state_code']
        dictionary["state_code"] = state_code
        user5 = session.query(User).filter(User.email==str(email)).update({'state_code':state_code})
     except:
        state_code = session.query(User).filter(User.email==str(email)).first().state_code
        dictionary["state_code"] = state_code
        pass
     try:
        latitude = data['lat']
        dictionary["latitude"] = latitude
        user6 = session.query(User).filter(User.email==str(email)).update({'lat':latitude})
     except:
        latitude = session.query(User).filter(User.email==str(email)).first().latitude
        dictionary["latitude"] = latitude
        pass
     try:
        longitude = data['lng']
        dictionary["longitude"] = longitude
        user7 = session.query(User).filter(User.email==str(email)).update({'lng':longitude})
     except:
        longitude = session.query(User).filter(User.email==str(email)).first().longitude
        dictionary["longitude"] = longitude
        pass
     try:
        userid = session.query(User).filter(User.email==str(email)).first().id
        base_64_photo_data = data['base_64_photo_data']
        f = open("/photos/"+str(userid),"w")
        f.write(base_64_photo_data)
        f.close()
     except:
        base_64_photo_data = session.query(User).filter(User.email==str(email)).first().base_64_photo_data
        dictionary["base_64_photo_data"] = base_64_photo_data
        pass
     try:
        photo_thumb_url = data['photo_thumb_url']
        dictionary["photo_thumb_url"] = photo_thumb_url
        user9 = session.query(User).filter(User.email==str(email)).update({'photo_thumb_url':photo_thumb_url})
     except:
        #TODO: Sort out photo_thumb_url
        #photo_thumb_url = session.query(User).filter(User.email==str(email)).first().photo_thumb_url
        pass
     try:
        photo_large_url = data['photo_large_url']
        dictionary["photo_large_url"] = photo_large_url
        user10 = session.query(User).filter(User.email==str(email)).update({'photo_large_url':photo_large_url})
     except:
        #TODO: Sort out large photo url
       # photo_large_url = session.query(User).filter(User.email==str(email)).first().photo_large_url
        pass

     return dictionary


@app.route("/users/me",methods=["PATCH"])
def userMePatchNationBuilder():
    print("Entering user me patch handler")
    #request object has following attributes that can be updated:
    #email,password,first_name,last_name,state_code,lat,lng,base_64_photo_data
    returnedOAuthVerification = oauthVerification(request=request)
    if(returnedOAuthVerification==True):
        pass
    else:
        return 401

    json = request.get_json()
    data = json['data']['attributes']
    email = data['email']
    user = session.query(User).filter(User.email==str(email)).update({'email':email})
    paramsdict = userMePatchTryExceptParams(data=data,email=email)
    print(paramsdict, "THIS IS WHAT YOU ARE LOOKING FOR SLUDDS")
    password = paramsdict["password"]
    first_name = paramsdict["first_name"]
    last_name = paramsdict["last_name"]
    state_code = paramsdict["state_code"]
    latitude = paramsdict["latitude"]
    longitude = paramsdict["longitude"]
    photo_thumb_url = "None"
    photo_large_url = "None"

    session.commit()
    query = session.query(User).all()
    for user in query:
        if(user.email==email):
            id = user.id
            attributes = helperfunctions.userMePatchHandlerAttributes(email=email,password=password,first_name=first_name,last_name=last_name,state_code=state_code,
                latitude=latitude,longitude=longitude,photo_thumb_url=photo_thumb_url,photo_large_url=photo_large_url)
            relationships = helperfunctions.userMePatchHandlerRelationships(userid=1,attributes=attributes,followerid=1,followingid=1)
            data = {"id":id,"type":'users',"attributes":attributes,"relationships":relationships}
            response = jsonify(data=data)
            printRequestTime()
            return response
    #case where no user matched
    return 401


@app.route("/users/:id",methods=["GET"])
def useridHandlerNationBuilder():
    pass

@app.route('/users/me',methods=["GET"])
def usersmeGETHandler():
    print("Entering users me get handler")
    returnedOAuthVerification = oauthVerification(request=request)
    if(returnedOAuthVerification==True):
        pass
    else:
        print("Authentication has failed")
        return 401
    bearerToken = request.environ['HTTP_AUTHORIZATION'].split(" ")[1]
    
    user = session.query(User).filter(User.remember_token==int(bearerToken)).first()
    #Check to see if user valid, if they are return post requset in same format of /users
    #Convert first entry in list into the user object
    print('Before user if statement')
    if user != None:
        attributes = helperfunctions.useridHandlerAttributes(email=user.email,password=user.encrypted_password,first_name=user.first_name,last_name=user.last_name,state_code=user.state_code,
            latitude=user.longitude,longitude=user.latitude,photo_thumb_url="None",photo_large_url="None")
        print("Before user relationships")
        relationships = helperfunctions.useridHandlerRelationships(userid=1,followerid=1,followingid=1)
        data = {"id":user.id,"type":'users',"attributes":attributes,"relationships":relationships}
        print("After user data")
        response = jsonify(data=data)
        printRequestTime()
        return response
    else:
        #User is unauthenticated
        return {"message": "Unauthenticated"}, 401






@app.route('/addresses',methods=["GET"])
def addressResponseNationBuilder():
    print("Entering addresses get handler")
    #Address protocal allows for each single or multiple based on presence of radius param 
    returnedOAuthVerification = oauthVerification(request=request)
    if(returnedOAuthVerification==True):
        pass
    else:
        print("Authentication has failed")
        return 401
    longitude = request.args.get('longitude')
    latitude = request.args.get('latitude')
    locationCoordinates = (latitude,longitude)
    if(request.args.get("radius")!=None):
        #Multiple Address Case
        radius = request.args.get("radius")
        radius = int(radius)
        #Check for valid nationbuilder access_token
        if(globalVariables.nationBuilderAccessToken==""):
            helperfunctions.updateNationBuilderAccessToken()
        nationBuilderRequestMain = helperfunctions.nationBuilderAddressRequestHandler(nationBuilderSlug = globalVariables.nationBuilderSlug , nationBuilderAccessToken = globalVariables.nationBuilderAccessToken)
        walkListID = helperfunctions.fetchWalkListIDFromNationBuilderAddressRequest(request = nationBuilderRequestMain)
        helperfunctions
        if(walkListID==None):
            return {"message":"Slug not found"}, 401
        nationBuilderAddressRequest = helperfunctions.getNationBuilderAddresses(walklistid = walkListID , nationBuilderSlug = globalVariables.nationBuilderSlug , nationBuilderAccessToken = globalVariables.nationBuilderAccessToken)
        #iaacsilberbergdev.nationbuilder.com/api/v1/lists?limit=10&__proto__=&access_token=ae8b060f8dcfabaec1fca6a08ddbd0c613c1eb52dae0015c00ea975d48005e3b
        #Get walklist from nationbuilder
        listOfAddresses = helperfunctions.findAddressesFromNationBuilderRequest(request = nationBuilderAddressRequest)
        returnableResponse = []
        for address in listOfAddresses:
            attributes = helperfunctions.addressHandlerGETAttributes(longitude=address["longitude"],latitude=address["latitude"],street_1=address["street_1"],street_2=address["street_2"],city=address["city"],
                    state_code=address["state_code"],zip_code=address["zip_code"],visited_at=address["visited_at"],best_canvass_response=address["best_canvass_response"],
                    last_canvass_response=address["last_canvass_response"])
            returnableResponse.append({"id":address["id"],"type":"addresses","attributes" : attributes})
        printRequestTime()
        return jsonify(data=returnableResponse)
    else:
        #Single Address Case
        #This is the case where there is no radius argument passed
        street_1 = request.args.get("street_1")
        street_2 = request.args.get("street_2")
        city = request.args.get("city")
        state_code = request.args.get("state_code")
        zip_code = request.args.get("zip_code")
        if(globalVariables.environment == "testing"):
            timeString = "1970-06-15T07:06:06.003Z"
        elif(globalVariables.environment == "production"):
            timeString = time.strftime("%Y-%m-%d" + "T" + "%H:%M:%S"+".001Z")
        #yyyy-MM-dd'T'HH:mm:ss.SSS'Z
        attributes = helperfunctions.addressHandlerGETAttributes(longitude=longitude,latitude = latitude,street_1 = street_1, street_2 = street_2, city = city, zip_code = zip_code,state_code=state_code,
            visited_at=timeString,best_canvass_response = "not_yet_visited",last_canvass_response = "This was a really good last canvass response")
        printRequestTime()
        return jsonify(data=[{"id":1,"type":"addresses","attributes":attributes}],included=[])


@app.route("/visits",methods=["POST"])
def visitsResponseNationBuilder():
    print("Entering visits post handler")
    returnedOAuthVerification = oauthVerification(request=request)
    if(returnedOAuthVerification==True):
        pass
    else:
        print("Authentication has failed")
        return 401
    json = request.get_json()
    bearerToken = request.environ['HTTP_AUTHORIZATION'].split(" ")[1]
    #Search through users to find one matchinig bearerToken
    loggedInUser = session.query(User).filter(User.remember_token==bearerToken).first()
    durationInSeconds = json['data']['attributes']['duration_sec']
    includedList = json['included']
    for i in includedList:
        if(i['type']=="people"):
            pass
        elif(i['type']=="addresses"):
            pass
        else:
            print("Visit includedList contains an object not of type people or addresses")
    if(globalVariables.environment == "testing"):
        timeString = "1970-06-15T07:06:06.003Z"
    if(globalVariables.environment == "production"):
        timeString = time.strftime("%Y-%m-%d" + "T" + "%H:%M:%S"+".001Z")
    helperfunctions.nationBuilderSurveyQuestionHandler(includedList)
    attributes = {"duration_sec":durationInSeconds,"total_points":loggedInUser.total_points,"created_at":timeString}
    relationships =helperfunctions.visitHandlerRelationships(userid=2,addressupdateid=1,addressid=1,scoreid=1)
    scoreObject =helperfunctions.visitHandlerScoreObject(id=1,updatePoints=globalVariables.updatePoints,doorKnockPoints=globalVariables.doorKnockPoints,relationshipsID=1)
    #update the users totalpoints
    loggedInUser.total_points += helperfunctions.visitHandlerScore(globalVariables.doorKnockPoints,globalVariables.updatePoints)
    session.commit()
    printRequestTime()
    return jsonify(data = {"attributes":attributes,"relationships":relationships}, included = [scoreObject])


@app.route("/rankings",methods=["GET"])
def rankingsResponseNationBuilder():
    print("Entering /rankings")
    returnedOAuthVerification = oauthVerification(request=request)
    if(returnedOAuthVerification==True):
        pass
    else:
        print("Authentication has failed")
        return 401
    typeOfRequest = request.args.get("type")
    userBearerToken = request.headers['Authorization'].split(" ")[1]
    if(typeOfRequest== "everyone"):
        print("request is everyone")
        userList = session.query(User).order_by(desc(User.total_points)).all()
        #check remember token
        collectedUserDicts = []
        data = []
        for index,value in enumerate(userList):
            if(value.remember_token==userBearerToken):
                for j in range(-5,5):
                    if(index+j) >= 0:
                        try:
                            user = userList[index+j]
                            attributes = helperfunctions.rankingHandlerAttributes(email=user.email,password=user.encrypted_password,first_name=user.first_name,last_name=user.last_name,
                                state_code=user.state_code,latitude=user.latitude,longitude=user.longitude,photo_thumb_url="None",photo_large_url="None")
                            collectedUserDicts.append({"id":user.id,"type":"users","attributes":attributes})
                            dataattributes = {"rank":index+1,"score": user.total_points}
                            datarelationships = {"user":{"data":{"id":user.id,"type":"users"}}}
                            data.append({"id":15+j,"type":"rankings","attributes":dataattributes, "relationships":datarelationships})
                        except:
                            pass
        return jsonify(data = data , included = collectedUserDicts)
    elif(typeOfRequest == "state"):
        print("request is state")
        userList = session.query(User).order_by(desc(User.total_points)).all()
        #check remember token
        collectedUserDicts = []
        data = []
        for index,value in enumerate(userList):
            if(value.remember_token==userBearerToken):
                for j in range(-5,5):
                    if(index+j) >= 0:
                        try:
                            user = userList[index+j]
                            attributes = helperfunctions.rankingHandlerAttributes(email=user.email,password=user.encrypted_password,first_name=user.first_name,last_name=user.last_name,
                                state_code=user.state_code,latitude=user.latitude,longitude=user.longitude,photo_thumb_url="None",photo_large_url="None")
                            collectedUserDicts.append({"id":user.id,"type":"users","attributes":attributes})
                            dataattributes = {"rank":index+1,"score": user.total_points }
                            datarelationships = {"user":{"data":{"id":user.id,"type":"users"}}}
                            data.append({"id":15+j,"type":"rankings","attributes":dataattributes, "relationships":datarelationships})
                        except:
                            pass
        return jsonify(data = data , included = collectedUserDicts)
    elif(typeOfRequest == "friends"):
        print("request is friends")
        userList = session.query(User).order_by(desc(User.total_points)).all()
        #check remember token
        collectedUserDicts = []
        data = []
        for index,value in enumerate(userList):
            if(value.remember_token==userBearerToken):
                for j in range(-5,5):
                    if(index+j) >= 0:
                        try:
                            user = userList[index+j]
                            attributes = helperfunctions.rankingHandlerAttributes(email=user.email,password=user.encrypted_password,first_name=user.first_name,last_name=user.last_name,
                                state_code=user.state_code,latitude=user.latitude,longitude=user.longitude,photo_thumb_url="None",photo_large_url="None")
                            collectedUserDicts.append({"id":user.id,"type":"users","attributes":attributes})
                            dataattributes = {"rank":index+1,"score": user.total_points }
                            datarelationships = {"user":{"data":{"id":user.id,"type":"users"}}}
                            data.append({"id":15+j,"type":"rankings","attributes":dataattributes, "relationships":datarelationships})
                        except:
                            pass
        return jsonify(data = data , included = collectedUserDicts)
    else:
        pass


@app.route("/devices",methods=["POST"])
def devicesResponseNationBuilder():
    print("Enterting /devices")
    returnedOAuthVerification = oauthVerification(request=request)
    if(returnedOAuthVerification==True):
        pass
    else:
        print("Authentication has failed")
        return 401
    printRequestTime()
    pass



'''
Ping works by sending a request to people/count endpoint and seeing what the type of response is
'''
@app.route("/ping",methods=["GET"])
def pingNationbuilder():
    print("Entering ping handler")
    returnedOAuthVerification = oauthVerification(request=request)
    if(returnedOAuthVerification==True):
        printRequestTime()
        return jsonify(ping = CURRENT_USER_EMAIL)
    else:
        printRequestTime()
        return jsonify(ping = "Pong!")

def nationBuilderMain():
    print("Starting nationbuilder server")
    app.run(host="0.0.0.0", port = 5000, processes = globalVariables.numberOfProcesses)
    print("Terminating nationbuilder server")
 

if __name__ == "__main__":
    nationBuilderMain()
