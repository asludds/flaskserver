import requests
from flaskserver import globalVariables
from flaskserver import helperfunctions
from flask_sqlalchemy import SQLAlchemy
from flask import Flask, url_for, session, request, jsonify, g
from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Float, Boolean, desc
from sqlalchemy.orm import sessionmaker, relationship
import geopy.distance as distance
import base64
import random
from werkzeug.security import generate_password_hash , check_password_hash
import os
import pprint
import time
import logging
import json as jsonlib
from sqlalchemy.ext.declarative import declarative_base
from civicrm.civicrmhelperfunctions import *
from helperfunctions import *




Base = declarative_base()

os.environ['DEBUG'] = globalVariables.OS_DEBUG
os.environ['OAUTHLIB_INSECURE_TRANSPORT'] = globalVariables.OAUTHLIB_INSECURE_TRANSPORT

engine = create_engine(globalVariables.civiCRMDatabaseURI, echo=True)
Session = sessionmaker(bind=engine)
session = Session()

app = Flask(__name__)
app.debug = globalVariables.debugBoolean
app.config['SQLALCHEMY_DATABASE_URI'] = globalVariables.civiCRMDatabaseURI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = globalVariables.SQLALCHEMY_TRACK_MODIFICATIONS

class User(Base):
    __tablename__ = "users"
    id = Column(Integer,primary_key=True,unique=True) #NOTE this is the civicrm server id
    encrypted_password = Column(String)
    remember_token = Column(String)
    facebook_id = Column(String)
    facebook_access_token = Column(String)
    photo_file_name = Column(String)
    photo_content_type = Column(String)
    photo_file_size = Column(Integer)
    photo_updated_at = Column(Integer)
    base_64_photo_data = Column(String) #This represents the address for the saved photo data

Base.metadata.create_all(bind=engine)


@app.before_request
def before_request():
    g.request_start_time = time.time()
    g.request_time = lambda: "%.5fs" % (time.time() - g.request_start_time)

def printRequestTime():
    print("Request was completed in " + g.request_time())

def oauthVerification(request):
    print("Entering oauthVerification")
    try:
        authorization = request.headers['Authorization']

    except:
        return jsonify({"message": "Unauthenticated"}), 401
    #check to see if already authorized
    if(authorization[0:6]=="Bearer"):
        return True
    decodedClientID, decodedClientSecret = base64ClientIDClientSecretDecoder(authorization=authorization.split(" ")[1])

    if(str(globalVariables.CLIENT_ID)==decodedClientID and str(globalVariables.CLIENT_SECRET)==decodedClientSecret):
        return True
    else:
        return False

@app.before_request
def log_request():
    app.logger.debug("Request Headers %s", request.headers)
    return None

@app.route('/oauth/token',methods=['POST'])
def oauthHandler():
    logging.info("Entering oauthHandler")
    returnedOAuthVerification = oauthVerification(request=request)
    if(returnedOAuthVerification):
        username = request.form['username'] #Note that the username is the email attribute
        password = request.form['password']
        userID = findUserIDByEmail(str(username))
        logging.debug("Username: ", username)
        logging.debug("Password: ", password)
        logging.debug("UserID: ",userID)
        userInDatabase = session.query(User).filter(User.id==userID).first()
        passwordInDatabase = userInDatabase.encrypted_password
        passwordCheckBoolean = check_password_hash(pwhash=passwordInDatabase,password=str(password))
        if(passwordCheckBoolean==True):
            access_token = random.randrange(globalVariables.civiCRMIDUpperBound)
            user = session.query(User).filter(User.id==userID).update({'remember_token':access_token})
            session.commit()
            json = jsonify(access_token = access_token,expires_in = globalVariables.oauthTimeLimitSeconds, token_type = "bearer", user_id = 1, created_at = 0)
            printRequestTime()
            return json
        else:
            logging.info("Password received by oauthHandler was not correct for user", username)
            return 401
    else:
        logging.info("Authentication has failed.")
        return 401

#This is for the creation of a user
@app.route('/users',methods=["POST"])
def usersHandler():
    logging.info("Entering user post handler")
    json = request.get_json()
    data = json['data']['attributes']
    hashed_password = generate_password_hash(data['password'], method = 'sha512')
    userid = random.randrange(globalVariables.civiCRMIDUpperBound)
    #Check that userid is not already taken
    while True:
        useridquery = session.query(User).filter(User.id==userid).first()
        if(useridquery==None):
            break
        else:
            print("Generating another random number")
            userid = random.randrange(globalVariables.civiCRMIDUpperBound)
    state_code = findStateGivenLatLng(latitude=data["lat"],longitude=data["lng"])
    if(data["email"]!=None):
        first_name = data['first_name']
        last_name = data['last_name']
        email = data['email']
        home_state = "NA"
        total_points = 0
        state_code = str(state_code)
        visits_count = 0
        latitude = data['lat']
        longitude = data['lng']
        #This is the case where someone signs up with emails
        newuser = User(id=userid, encrypted_password = hashed_password, remember_token = "None", facebook_id = "None",
            facebook_access_token = "None", photo_file_name = "None",photo_content_type = "None", photo_file_size = 0,
            photo_updated_at = 0, base_64_photo_data = "/photos/"+str(userid))
        userCreationBool = createUser(contact_id=userid,first_name=first_name,last_name=last_name,email=email)
        print("Was the user created?",userCreationBool)
    elif(data["facebook_access_token"] != None and data["facebook_id"]!=None):
        #This is the case where someone signs up with facebook
        pass
    else:
        print("Request did not have attributes email, facebook_access_token, or facebook_id")

    session.add(newuser)
    session.commit()
    attributes = userHandlerPOSTAttributes(email=data["email"],password=data["password"],first_name=data["first_name"],last_name=data["last_name"],
        state_code=str(state_code),latitude=data["lat"],longitude=data["lng"])
    relationships = userHandlerPOSTRelationships()
    data = userHandlerPOSTData(userid = userid,attributes = attributes,relationships = relationships)
    printRequestTime()
    return jsonify(data = data)



def userMePatchTryExceptParams(data,email):
     dictionary = {}
     try:
        password = data['password']
        dictionary["password"] = password
        user2 = session.query(User).filter(User.email==str(email)).update({'password':password})
     except:
          password = session.query(User).filter(User.email==str(email)).first().encrypted_password
          dictionary['password'] = password
          pass
     try:
        first_name = data['first_name']
        dictionary["first_name"] = first_name
        user3 = session.query(User).filter(User.email==str(email)).update({'first_name':first_name})
     except:
        first_name = session.query(User).filter(User.email==str(email)).first().first_name
        dictionary["first_name"] = first_name
        pass    
     try:
        last_name = data['last_name']
        dictionary["last_name"] = last_name
        user4 = session.query(User).filter(User.email==str(email)).update({'last_name':last_name})
     except:
        last_name = session.query(User).filter(User.email==str(email)).first().last_name
        dictionary["last_name"] = last_name
        pass
     try:
        state_code = data['state_code']
        dictionary["state_code"] = state_code
        user5 = session.query(User).filter(User.email==str(email)).update({'state_code':state_code})
     except:
        state_code = session.query(User).filter(User.email==str(email)).first().state_code
        dictionary["state_code"] = state_code
        pass
     try:
        latitude = data['lat']
        dictionary["latitude"] = latitude
        user6 = session.query(User).filter(User.email==str(email)).update({'lat':latitude})
     except:
        latitude = session.query(User).filter(User.email==str(email)).first().latitude
        dictionary["latitude"] = latitude
        pass
     try:
        longitude = data['lng']
        dictionary["longitude"] = longitude
        user7 = session.query(User).filter(User.email==str(email)).update({'lng':longitude})
     except:
        longitude = session.query(User).filter(User.email==str(email)).first().longitude
        dictionary["longitude"] = longitude
        pass
     try:
        userid = session.query(User).filter(User.email==str(email)).first().id
        base_64_photo_data = data['base_64_photo_data']
        f = open("/photos/"+str(userid),"w")
        f.write(base_64_photo_data)
        f.close()
     except:
        base_64_photo_data = session.query(User).filter(User.email==str(email)).first().base_64_photo_data
        dictionary["base_64_photo_data"] = base_64_photo_data
        pass
     try:
        photo_thumb_url = data['photo_thumb_url']
        dictionary["photo_thumb_url"] = photo_thumb_url
        user9 = session.query(User).filter(User.email==str(email)).update({'photo_thumb_url':photo_thumb_url})
     except:
        #TODO: Sort out photo_thumb_url
        #photo_thumb_url = session.query(User).filter(User.email==str(email)).first().photo_thumb_url
        pass
     try:
        photo_large_url = data['photo_large_url']
        dictionary["photo_large_url"] = photo_large_url
        user10 = session.query(User).filter(User.email==str(email)).update({'photo_large_url':photo_large_url})
     except:
        #TODO: Sort out large photo url
       # photo_large_url = session.query(User).filter(User.email==str(email)).first().photo_large_url
        pass

     return dictionary


@app.route('/users/me',methods=["PATCH"])
def usersmePATCHHandler():
    print("Entering user me patch handler")
    #request object has following attributes that can be updated:
    #email,password,first_name,last_name,state_code,lat,lng,base_64_photo_data
    returnedOAuthVerification = oauthVerification(request=request)
    if(returnedOAuthVerification==True):
        pass
    else:
        return 401

    json = request.get_json()
    data = json['data']['attributes']
    email = data['email']
    user = session.query(User).filter(User.email==str(email)).update({'email':email})
    paramsdict = userMePatchTryExceptParams(data=data,email=email)
    print(paramsdict, "THIS IS WHAT YOU ARE LOOKING FOR SLUDDS")
    password = paramsdict["password"]
    first_name = paramsdict["first_name"]
    last_name = paramsdict["last_name"]
    state_code = paramsdict["state_code"]
    latitude = paramsdict["latitude"]
    longitude = paramsdict["longitude"]
    photo_thumb_url = "None"
    photo_large_url = "None"
    session.commit()
    query = session.query(User).all()
    for user in query:
        if(user.email==email):
            id = user.id
            attributes = userMePatchHandlerAttributes(email=email,password=password,first_name=first_name,last_name=last_name,state_code=state_code,
                latitude=latitude,longitude=longitude,photo_thumb_url=photo_thumb_url,photo_large_url=photo_large_url)
            relationships = userMePatchHandlerRelationships(userid=1,attributes=attributes,followerid=1,followingid=1)
            data = {"id":id,"type":'users',"attributes":attributes,"relationships":relationships}
            response = jsonify(data=data)
            printRequestTime()
            return response
    #case where no user matched
    return 401 

@app.route('/users/:id',methods=["GET"])
def useridHandler():
    print("Entering users id get handler")
    #Retrieves a user by id, no authentication needed
    userID = int(str(request.url_rule).split("/users/")[1])
    user = session.query(User).filter(User.id==userID).first()
    userEmail = getUserEmail(userID)
    userFirstName = getUserFirstName(userID)
    userLastName = getUserLastName(userID)
    userStateCode = getUserStateCode(userID)
    userLatitude = getUserLatitude(userID)
    userLongitude = getUserLongitude(userID)
    attributes = useridHandlerAttributes(email=userEmail,password=user.encrypted_password,first_name=userFirstName,last_name=userLastName,
        state_code=userStateCode,latitude=userLatitude,longitude=userLongitude,photo_thumb_url="None",photo_large_url="None")
    relationships = {"visits":[],"followers":[],"following":[]}
    data = {"id":id,"type":'users',"attributes":attributes,"relationships":relationships}
    printRequestTime()
    return jsonify(data = data)

@app.route('/users/me',methods=["GET"])
def usersmeGETHandler():
    print("Entering users me get handler")
    returnedOAuthVerification = oauthVerification(request=request)
    if(returnedOAuthVerification==True):
        pass
    else:
        print("Authentication has failed")
        return 401
    bearerToken = request.environ['HTTP_AUTHORIZATION'].split(" ")[1]
    
    user = session.query(User).filter(User.remember_token==int(bearerToken)).first()
    #Check to see if user valid, if they are return post requset in same format of /users
    #Convert first entry in list into the user object
    userID = user.id
    userEmail = getUserEmail(userID)
    userFirstName = getUserFirstName(userID)
    userLastName = getUserLastName(userID)
    try:
        userStateCode = getUserStateCode(userID)
    except:
        print("REMINDER TO FIX THE USER STATE CODE")
        userStateCode = "MA"
    try:
        userLatitude = getUserLatitude(userID)
    except:
        userLatitude = 42.369827
    try:
        userLongitude = getUserLongitude(userID)
    except:
        userLongitude = -71.100090
    if user != None:
        attributes = useridHandlerAttributes(email=userEmail,password=user.encrypted_password,first_name=userFirstName,last_name=userLastName,
            state_code=userStateCode,latitude=userLatitude,longitude=userLongitude,photo_thumb_url="None",photo_large_url="None")
        relationships = useridHandlerRelationships(userid=1,followerid=1,followingid=1)
        data = {"id":userID,"type":'users',"attributes":attributes,"relationships":relationships}
        response = jsonify(data=data)
        printRequestTime()
        return response
    else:
        #User is unauthenticated
        return {"message": "Unauthenticated"}, 401

@app.route('/addresses',methods=["GET"])
def addressResponse():
    print("Entering addresses get handler")
    #Address protocal allows for each single or multiple based on presence of radius param 
    # returnedOAuthVerification = oauthVerification(request=request)
    # if(returnedOAuthVerification==True):
    #     pass
    # else:
    #     print("Authentication has failed")
    #     return 401
    # longitude = request.args.get('longitude')
    # latitude = request.args.get('latitude')
    latitude = 42.369827
    longitude = -71.109349
    locationCoordinates = (latitude,longitude)
    if(request.args.get("radius")!=None):
        #Multiple Address Case
        # radius = request.args.get("radius")
        radius = 1000
        radius = int(radius)
        addresses = getAddressesInSquare(lowerlatitude=-90,upperlatitude=90,lowerlongitude=-90,upperlongitude=90)
        returnableResponse = []
        print(addresses,len(addresses))
        for address in addresses:
            addressCoordinates = (address["geo_code_1"],address["geo_code_2"])
            stateCode = stateIDToStateAbreviation(address["state_province_id"])
            if(distance.distance(locationCoordinates,addressCoordinates).meters<=radius):
                attributes = addressHandlerGETAttributes(longitude=address["geo_code_2"],latitude=address["geo_code_1"],
                    street_1=address["street_address"],street_2=" ",city=address["city"],
                    state_code=stateCode,zip_code=address["postal_code"],visited_at=0,
                    best_canvass_response="best_canvass_response",
                    last_canvass_response="last_canvass_response")
                returnableResponse.append({"id":address['id'],"type":"addresses","attributes" : attributes})
        printRequestTime()
        return jsonify(data=returnableResponse)

    else:
        #Single Address Case
        #This is the case where there is no radius argument passed
        # street_1 = request.args.get("street_1")
        # street_2 = request.args.get("street_2")
        # city = request.args.get("city")
        # state_code = request.args.get("state_code")
        # zip_code = int(request.args.get("zip_code"))
        if(globalVariables.environment == "testing"):
            timeString = "1970-06-15T07:06:06.003Z"
        elif(globalVariables.environment == "production"):
            timeString = time.strftime("%Y-%m-%d" + "T" + "%H:%M:%S"+".001Z")
        #yyyy-MM-dd'T'HH:mm:ss.SSS'Z
        attributes = addressHandlerGETAttributes(longitude=longitude,latitude = latitude,street_1 = street_1, street_2 = street_2, city = city,
            zip_code = zip_code,state_code=state_code,visited_at=timeString,
            best_canvass_response = "not_yet_visited",last_canvass_response = "This was a really good last canvass response")
        printRequestTime()
        return jsonify(data=[{"id":1,"type":"addresses","attributes":attributes}],included=[])
    

@app.route('/visits',methods=["POST"])
def visitsResponse():
    print("Entering visits post handler")
    returnedOAuthVerification = oauthVerification(request=request)
    if(returnedOAuthVerification==True):
        pass
    else:
        print("Authentication has failed")
        return 401
    json = request.get_json()
    bearerToken = request.environ['HTTP_AUTHORIZATION'].split(" ")[1]
    #Search through users to find one matchinig bearerToken
    loggedInUser = session.query(User).filter(User.remember_token==bearerToken).first()
    durationInSeconds = json['data']['attributes']['duration_sec']
    includedList = json['included']
    for i in includedList:
        if(i['type']=="people"):
            pass
        elif(i['type']=="addresses"):
            pass
        else:
            print("Visit includedList contains an object not of type people or addresses")
    if(globalVariables.environment == "testing"):
        timeString = "1970-06-15T07:06:06.003Z"
    if(globalVariables.environment == "production"):
        timeString = time.strftime("%Y-%m-%d" + "T" + "%H:%M:%S"+".001Z")
    loggedInUserID = loggedInUser.id
    currentUserScore = getUserTotalPoints(contactid=loggedInUserID)
    attributes = {"duration_sec":durationInSeconds,"total_points":currentUserScore,"created_at":timeString}
    relationships = visitHandlerRelationships(userid=2,addressupdateid=1,addressid=1,scoreid=1)
    scoreObject = visitHandlerScoreObject(id=1,updatePoints=globalVariables.updatePoints,doorKnockPoints=globalVariables.doorKnockPoints,
        relationshipsID=1)
    #update the users totalpoints
    changeInScore = visitHandlerScore(globalVariables.doorKnockPoints,globalVariables.updatePoints)
    civicrm.updateUserTotalPoints(contactid=str(loggedInUserID),newpointsamount=str(int(currentUserScore) + changeInScore))
    #loggedInUser.total_points += visitHandlerScore(globalVariables.doorKnockPoints,globalVariables.updatePoints)
    #session.commit()
    printRequestTime()
    return jsonify(data = {"attributes":attributes,"relationships":relationships}, included = [scoreObject])


@app.route('/rankings',methods = ["GET"])
def rankingsResponse():
    print("Entering /rankings")
    returnedOAuthVerification = oauthVerification(request=request)
    if(returnedOAuthVerification==True):
        pass
    else:
        print("Authentication has failed")
        return 401
    typeOfRequest = request.args.get("type")
    userBearerToken = request.headers['Authorization'].split(" ")[1]
    if(typeOfRequest== "everyone"):
        print("request is everyone")
        userList = session.query(User).order_by(desc(User.total_points)).all()
        #check remember token
        collectedUserDicts = []
        data = []
        for index,value in enumerate(userList):
            if(value.remember_token==userBearerToken):
                for j in range(-5,5):
                    if(index+j) >= 0:
                        try:
                            user = userList[index+j]
                            attributes = rankingHandlerAttributes(email=user.email,password=user.encrypted_password,first_name=user.first_name,last_name=user.last_name,
                                state_code=user.state_code,latitude=user.latitude,longitude=user.longitude,photo_thumb_url="None",photo_large_url="None")
                            collectedUserDicts.append({"id":user.id,"type":"users","attributes":attributes})
                            dataattributes = {"rank":15+j,"score":320+j}
                            datarelationships = {"user":{"data":{"id":user.id,"type":"users"}}}
                            data.append({"id":15+j,"type":"rankings","attributes":dataattributes, "relationships":datarelationships})
                        except:
                            pass
        return jsonify(data = data , included = collectedUserDicts)
    elif(typeOfRequest == "state"):
        print("request is state")
        userList = session.query(User).order_by(desc(User.total_points)).all()
        #check remember token
        collectedUserDicts = []
        data = []
        for index,value in enumerate(userList):
            if(value.remember_token==userBearerToken):
                for j in range(-5,5):
                    if(index+j) >= 0:
                        try:
                            user = userList[index+j]
                            attributes = rankingHandlerAttributes(email=user.email,password=user.encrypted_password,first_name=user.first_name,last_name=user.last_name,
                                state_code=user.state_code,latitude=user.latitude,longitude=user.longitude,photo_thumb_url="None",photo_large_url="None")
                            collectedUserDicts.append({"id":user.id,"type":"users","attributes":attributes})
                            dataattributes = {"rank":15+j,"score":320+j}
                            datarelationships = {"user":{"data":{"id":user.id,"type":"users"}}}
                            data.append({"id":15+j,"type":"rankings","attributes":dataattributes, "relationships":datarelationships})
                        except:
                            pass
        return jsonify(data = data , included = collectedUserDicts)
    elif(typeOfRequest == "friends"):
        print("request is friends")
        userList = session.query(User).order_by(desc(User.total_points)).all()
        #check remember token
        collectedUserDicts = []
        data = []
        for index,value in enumerate(userList):
            if(value.remember_token==userBearerToken):
                for j in range(-5,5):
                    if(index+j) >= 0:
                        try:
                            user = userList[index+j]
                            attributes = rankingHandlerAttributes(email=user.email,password=user.encrypted_password,first_name=user.first_name,last_name=user.last_name,
                                state_code=user.state_code,latitude=user.latitude,longitude=user.longitude,photo_thumb_url="None",photo_large_url="None")
                            collectedUserDicts.append({"id":user.id,"type":"users","attributes":attributes})
                            dataattributes = {"rank":15+j,"score":320+j}
                            datarelationships = {"user":{"data":{"id":user.id,"type":"users"}}}
                            data.append({"id":15+j,"type":"rankings","attributes":dataattributes, "relationships":datarelationships})
                        except:
                            pass
        return jsonify(data = data , included = collectedUserDicts)
    else:
        pass
 

@app.route('/devices',methods = ["POST"])
def devicesResponse():
    print("Enterting /devices")
    returnedOAuthVerification = oauthVerification(request=request)
    if(returnedOAuthVerification==True):
        pass
    else:
        print("Authentication has failed")
        return 401
    printRequestTime()
    pass

@app.route('/ping',methods=['GET'])
def pingResponse():
    print("Entering ping handler")
    returnedOAuthVerification = oauthVerification(request=request)
    if(returnedOAuthVerification==True):
        printRequestTime()
        return jsonify(ping = CURRENT_USER_EMAIL)
    else:
        printRequestTime()
        return jsonify(ping = "Pong!") 

@app.route('/compatibility',methods=["GET"])
def compatibilityResponse():
    print("Enterting /compatibility")
    version = request.args.get("version")
    if(version in globalVariables.compatibleVersions):
        printRequestTime()
        return jsonify(compatible=True)
    else:
        printRequestTime()
        return jsonify(compatible=False)

@app.route('/script',methods=["GET"])
def scriptResponse():
    print("Entering Script Response")
    scriptString = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum"
    return jsonify(script = scriptString)

@app.route('/fakeAddresses',methods=["GET"])
def fakeResponse():
    print("Entering fake addresses")
    address1 = {
        "id" : 1,
        "type" : 'addresses',
        "attributes" : {
            "latitude" : "42.376171",
            "longitude" : "-71.119466",
            "street_1" : "street1",
            "street_2" : "",
            "city" : "city1",
            "state_code" : "MA",
            "zip_code" : "02139",
            "visited_at" : "visitedat1",
            "best_canvass_response" : "best1",
            "last_canvass_response" : "last1"
        }
    }
    address2 = {
        "id" : 1,
        "type" : 'addresses',
        "attributes" : {
            "latitude" : "42.362549",
            "longitude" : " -71.098824",
            "street_1" : "street2",
            "street_2" : "",
            "city" : "city2",
            "state_code" : "MA",
            "zip_code" : "02139",
            "visited_at" : "visitedat1",
            "best_canvass_response" : "best1",
            "last_canvass_response" : "last1"
        }
    }
    address3 = {
        "id" : 1,
        "type" : 'addresses',
        "attributes" : {
            "latitude" : "42.371239",
            "longitude" : "-71.099596",
            "street_1" : "street3",
            "street_2" : "",
            "city" : "city3",
            "state_code" : "MA",
            "zip_code" : "02139",
            "visited_at" : "visitedat1",
            "best_canvass_response" : "best1",
            "last_canvass_response" : "last1"
        }
    }
    address4 = {
        "id" : 1,
        "type" : 'addresses',
        "attributes" : {
            "latitude" : "42.464943",
            "longitude" : "-71.103180",
            "street_1" : "street4",
            "street_2" : "",
            "city" : "city4",
            "state_code" : "MA",
            "zip_code" : "02139",
            "visited_at" : "visitedat1",
            "best_canvass_response" : "best1",
            "last_canvass_response" : "last1"
        }
    }
    addresses = [address1,address2,address3,address4]
    return jsonify(data = addresses)

@app.route('/turfs',methods=['GET'])
def turfHanlder():
    print(request)
    turf = ["Turf 1","Turf 2","Turf 3","Turf 4"]
    return jsonify(data = turf)


#This endpoint takes as an argument the group name of the turf that we want to canvass and returns the latlngs, names, addresses of the people within the turf
@app.route('/turfAddresses',methods=['GET'])
def turfAddressHandler():
    print(request)
    turfname = request.args.get("turfname")
    jsonString = {"sequential":1,"groups":{"IN":[turfname]}}
    jsonToReturn = jsonlib.dumps(jsonString)
    response = createCiviCRMGETRequest(params = {"entity" : "Contacts","json":jsonToReturn})
    print(response.json())
    print(response.headers)
    

    return jsonify(data = [])
    
def civiCRMMain():
     print("Starting civiCRM server")
     app.run(host="0.0.0.0", port = 5000, processes = globalVariables.numberOfProcesses)
     print("Terminating civiCRM server")


if __name__ == '__main__':
     civiCRMMain()
