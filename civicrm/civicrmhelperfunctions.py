import requests
from flaskserver.globalVariables import *
from flaskserver import helperfunctions

'''
CiviCRM oauth ideal path
Locally on this server we are going to store each users civicrm api key, server key etc. 
Initially the user does not have access to any addresses until the field manager cuts turf

mobile device sign up
canvassing server makes internal user
Auto-generate APIkey on canvassing server for the civicrm server
User create request sent to civicrm server
API-key for user set on civicrm server. This is done by me.
SIDENOTE: Canvassing server is a user with the ability to create and modify users
SIDENOTE: Do a validation check so that APIKEY does not collide with another valid APIKEY

'''
#json={"sequential":1,"return":"geo_code_1,geo_code_2","geo_code_1":{"BETWEEN":["lower","upper"]},"geo_code_2":{"BETWEEN":["lower","upper"]}}
def civiCRMJsonCreator(dictionary):
     returnString = "{"
     for index,value in enumerate(dictionary.keys()):
          if(index>0):
               if(type(dictionary[value])==str):
                    returnString += ',"' + str(value) + '":"' + str(dictionary[value]) + '"'
               if(type(dictionary[value])==int):
                    returnString += ',"' + str(value) + '":' + str(dictionary[value]) 
               if(type(dictionary[value])==dict):
                    try:
                         val = dictionary[value]["LIKE"]
                         returnString += ',"' + str(value) + '":' + '{"LIKE":"' + str(val) + '"}'  
                    except:
                         try:
                              val = dictionary[value]["options"]
                              returnString += ',"' + str(value) + '":' + '{"options":"' + str(val) + '"}'    
                         except:
                              try:
                                   val = dictionary[value]["BETWEEN"]
                                   returnString += ',"' + str(value) + '":' + '{"BETWEEN":["' + str(val[0]) + '","' + str(val[1]) + '"]}'
                              except:
                                   try:
                                        val = dictionary[value]["limit"]
                                        returnString += ',' + '"options":{' + '"limit":' + str(val) + '}'
                                   except Exception as e:
                                        print(e)
          else:
               if(type(dictionary[value])==str):
                    returnString += '"' + str(value) + '":"' + str(dictionary[value]) + '"'
               if(type(dictionary[value])==int):
                    returnString += '"' + str(value) + '":' + str(dictionary[value])
               if(type(dictionary[value])==dict):
                    try:
                         val = dictionary[value]["LIKE"]
                         returnString += '"' + str(value) + '":' + '{"LIKE":"' + str(val) + '"}'
                    except:
                         try:
                              val = dictionary[value]["options"]
                              returnString += '"' + str(value) + '":' + '{"options":"' + str(val) + '"}'
                         except:
                              try:
                                   val = dictionary[value]["BETWEEN"]
                                   returnString += '"' + str(value) + '":' + '{"BETWEEN":["' + str(val[0]) + '","' + str(val[1]) + '"]}'
                              except:
                                   try:
                                        val = dictionary[value]["limit"]
                                        returnString += '"options":{' + '"limit":' + str(val) + '}'
                                   except Exception as e:
                                        print(e)

     returnString += "}"
     return returnString



def createCiviCRMPOSTRequest(apikey = globalVariables.civiCRMAPIKEY,key=globalVariables.civiCRMKEY,params=None):
     return requests.post(globalVariables.civiCRMURL + globalVariables.civiCRMAPIAddress, params = params)

def createCiviCRMGETRequest(apikey = globalVariables.civiCRMAPIKEY,key=globalVariables.civiCRMKEY,params=None):
     return requests.get(globalVariables.civiCRMURL + globalVariables.civiCRMAPIAddress, params = params)

def responseHandlerValues(response):
     json = response.json()
     return json['values'][0]

def responseHandlerBatchValues(response):
     json = response.json()
     return json['values']

def responseHandlerError(response):
     if(response.json()["is_error"]==0 or response.json()["is_error"]=="0"):
          return True
     else:
          return False

def stateAbreviationToStateID(stateAbreviation,country_id=1228):
     #Please note that the above country_id is defaulted to the united states
     json = {'sequential':1,"return":"id","abbreviation":str(stateAbreviation),"country_id":country_id}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"StateProvince","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMPOSTRequest(params=params)
     values = responseHandlerValues(response)
     return values["id"]
'''

NOTE: The default country is the united states
'''
def stateIDToStateAbreviation(stateID,country_id=1228):
     json = {"sequential":1,"return":"abbreviation","id":stateID,"country_id":country_id}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"StateProvince","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMGETRequest(params=params)
     return responseHandlerValues(response)["abbreviation"]

def getUserAddressID(contactid):
     json = {'sequential':1,"return":"address_id","id":str(contactid)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Contact","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMPOSTRequest(params=params)
     values = responseHandlerValues(response)
     return values["address_id"]

def updateUserLatitude(contactid,latitude):
     #For a given user get their addressid
     addressID = getUserAddressID(contactid=contactid)
     #Then for that address update the latitude
     json = {'sequential':1,"contact_id":str(contactid),"geo_code_1":str(latitude),"location_type_id":"Home","id":str(addressID)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Address","action":"create",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMPOSTRequest(params=params)
     return responseHandlerError(response=response)

def getUserLatitude(contactid):
     json = {"sequential":1,"return":"geo_code_1","contact_id":str(contactid)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Address","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMGETRequest(params=params)
     return responseHandlerValues(response)["geo_code_1"]

def deleteAddress(addressid):
     json = {"sequential":1,"id":str(addressid)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Address","action":"delete",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMPOSTRequest(params=params)
     return responseHandlerError(response)

def updateUserLongitude(contactid,longitude):
     addressID = getUserAddressID(contactid=contactid)
     json = {'sequential':1,"contact_id":str(contactid),"geo_code_2":str(longitude),"location_type_id":"Home","id":str(addressID)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Address","action":"create",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMPOSTRequest(params=params)
     return responseHandlerError(response=response)

def getUserLongitude(contactid):
     json = {"sequential":1,"return":"geo_code_2","contact_id":str(contactid)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Address","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMGETRequest(params=params)
     return responseHandlerValues(response)["geo_code_2"]

def updateUserStateCode(contact_id,statecode):
     addressID = getUserAddressID(contactid=contactid)
     stateProvideID = stateAbreviationToStateID(stateAbreviation=statecode)
     json = {'sequential':1,"contact_id":str(contactid),"state_province_id":str(stateProvideID),"location_type_id":"Home","id":str(addressID)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Address","action":"create",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMPOSTRequest(params=params)
     return responseHandlerError(response)
     
def getUserStateCode(contactid):
     json = {"sequential":1,"return":"state_province_id","contact_id":str(contactid)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Address","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMGETRequest(params=params)
     return responseHandlerValues(response)["state_province_id"]

def updateUserFirstName(contactid,firstname):
     json = {'sequential':1,"id":str(contactid),"first_name":str(firstname)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Contact","action":"create",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMPOSTRequest(params=params)
     return responseHandlerError(response)

def getUserFirstName(contactid):
     json = {"sequential":1,"return":"first_name","id":str(contactid)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Contact","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMGETRequest(params=params)
     return responseHandlerValues(response)["first_name"]

def updateUserLastName(contactid,lastname):
     json = {'sequential':1,"id":str(contactid),"last_name":str(lastname)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Contact","action":"create",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMPOSTRequest(params=params)
     return responseHandlerError(response)

def getUserLastName(contactid):
     json = {"sequential":1,"return":"last_name","contact_id":str(contactid)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Contact","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMGETRequest(params=params)
     return responseHandlerValues(response)["last_name"]

def contactIDtoEmailID(contactid):
     json = {"sequential":1,"return":"email","id":str(contactid)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Contact","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMGETRequest(params=params)
     return responseHandlerValues(response)["email_id"]

def updateUserEmail(contactid,email):
     emailid = contactIDtoEmailID(contactid)
     json = {"sequential":1,"id":emailid,"email":str(email)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Email","action":"create",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMPOSTRequest(params=params)
     return responseHandlerError(response)

def getUserEmail(contactid):
     emailid = contactIDtoEmailID(contactid)
     json = {"sequential":1,"return":"email","id":str(emailid)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Email","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMGETRequest(params=params)
     return responseHandlerValues(response)["email"]

def updateUserVisitsCount(contactid,newvisitcount):
     json = {'sequential':1,"id":str(contactid),"custom_20":str(newvisitcount)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Contact","action":"create",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMPOSTRequest(params=params)
     return responseHandlerError(response)
 
def getUserVisitsCount(contactid):
     json = {"sequential":1,"return":"custom_20","id":str(contactid)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Contact","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMGETRequest(params=params)
     return responseHandlerValues(response)["custom_20"]

def updateUserTotalPoints(contactid,newpointsamount):
     json = {'sequential':1,"id":str(contactid),"custom_19":str(newpointsamount)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Contact","action":"create",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMPOSTRequest(params=params)
     return responseHandlerError(response)

def getUserTotalPoints(contactid):
     json = {"sequential":1,"return":"custom_19","contact_id":str(contactid)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Contact","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMGETRequest(params=params)
     return responseHandlerValues(response)["custom_19"]

def updateAddressLatitude(addressid,latitude):
     json = {'sequential':1,"id":str(addressid),"geo_code_1":str(latitude)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Address","action":"create",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMPOSTRequest(params=params)
     return responseHandlerError(response)

def getAddressLatitude(addressid):
     json = {"sequential":1,"return":"geo_code_1","id":str(addressid)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Address","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMGETRequest(params=params)
     return responseHandlerValues(response)["geo_code_1"]

def updateAddressLongitude(addressid,longitude):
     json = {'sequential':1,"id":str(addressid),"geo_code_2":str(longitude)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Address","action":"create",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMPOSTRequest(params=params)
     return responseHandlerError(response)

def getAddressLongitude(addressid):
     json = {"sequential":1,"return":"geo_code_2","id":str(addressid)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Address","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMGETRequest(params=params)
     return responseHandlerValues(response)["geo_code_2"]

def updateAddressStreetAddress(addressid,street1):
     json = {'sequential':1,"id":str(addressid),"street_address":str(street1)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Address","action":"create",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMPOSTRequest(params=params)
     return responseHandlerError(response)

def getAddressStreetAddress(addressid):
     json = {"sequential":1,"return":"street_address","id":str(addressid)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Address","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMGETRequest(params=params)
     return responseHandlerValues(response)["street_address"]

def updateAddressCity(addressid,city):
     json = {'sequential':1,"id":str(addressid),"city":str(city)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Address","action":"create",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMPOSTRequest(params=params)
     return responseHandlerError(response)

def getAddressCity(addressid):
     json = {"sequential":1,"return":"city","id":str(addressid)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Address","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMGETRequest(params=params)
     return responseHandlerValues(response)["city"]

def updateAddressStateCode(addressid,statecode):
     provinceID = stateAbreviationToStateID(statecode)
     json = {'sequential':1,"id":str(addressid),"state_province_id":str(provinceID)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Address","action":"create",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMPOSTRequest(params=params)
     return responseHandlerError(response)

def getAddressStateCode(addressid):
     json = {"sequential":1,"return":"state_province_id","id":str(addressid)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Address","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMGETRequest(params=params)
     return responseHandlerValues(response)["state_province_id"]

def updateAddressZipcode(addressid,zipcode):
     json = {'sequential':1,"id":str(addressid),"postal_code":str(zipcode)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Address","action":"create",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMPOSTRequest(params=params)
     return responseHandlerError(response)

def getAddressZipcode(addressid):
     json = {"sequential":1,"return":"postal_code","id":str(addressid)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Address","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMGETRequest(params=params)
     return responseHandlerValues(response)["postal_code"]

def updateVoterFirstName(voterid,firstname):
     json = {'sequential':1,"contact_sub_type":"Voter","id":str(voterid),"first_name":str(firstname)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Contact","action":"create",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMPOSTRequest(params=params)
     return responseHandlerError(response)

def getVoterFirstName(voterid):
     json = {"sequential":1,"return":"first_name","id":str(voterid)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Contact","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMGETRequest(params=params)
     return responseHandlerValues(response)["first_name"]

def updateVoterLastName(voterid,lastname):
     json = {'sequential':1,"contact_sub_type":"Voter","id":str(voterid),"last_name":str(lastname)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Contact","action":"create",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMPOSTRequest(params=params)
     return responseHandlerError(response)

def getVoterLastName(voterid):
     json = {"sequential":1,"return":"last_name","id":str(voterid)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Contact","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMGETRequest(params=params)
     return responseHandlerValues(response)["last_name"]

def voteridToPhoneID(voterid):
     json = {"sequential":1,"return":"phone","id":str(voterid)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Contact","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMGETRequest(params=params)
     return responseHandlerValues(response)["phone_id"]

def updateVoterPhone(voterid,phonenumber):
     phoneid = voteridToPhoneID(voterid)
     if(phoneid==""):
          #TODO
          pass
     else:
          json = {'sequential':1,"id":str(phoneid),"phone":str(phonenumber)}
          jsonString = civiCRMJsonCreator(json)
          params = {"entity":"Phone","action":"create",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
               "json":jsonString}
          response = createCiviCRMPOSTRequest(params=params)
          return responseHandlerError(response)

def getVoterPhone(voterid):
     json = {"sequential":1,"return":"phone","id":str(voterid)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Contact","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMGETRequest(params=params)
     return responseHandlerValues(response)["phone"]

def updateVoterEmail(voterid,email):
     pass

def getVoterEmail(voterid):
     pass

def updateVoterPrefferedContactMethod(voterid,prefferedContactMethod):
     pass

def getVoterPrefferedContactMethod(voterid):
     pass

def updateVoterLastVisitedByID(voterid,userid):
     pass

def getVoterLastVisitedByID(voterid):
     pass

def updateVoterPreviousParticipatedInPrimaryOrCaucus(voterid,primaryorcaucusbool):
     pass

def getVoterPreviousParticipatedInPrimaryOrCaucus(voterid):
     pass

def updateVoterPartyAffiliation(voterid,partyaffiliation):
     pass

def getVoterPartyAffiliation(voterid):
     pass

def updateVoterCanvassResponse(voterid,canvassResponse):
     pass

def getVoterCanvassResponse(voterid):
     pass

def updateVoterTimeLastVisited(voterid,timelastvisited):
     pass 

def getVoterTimeLastVisited(voterid):
     pass

'''
Takes in two lat longs that define a box on the map.
Returns a list of dicts containing latitude, longitude, city, postal_code, state_province_id,street_address
'''

#json={"sequential":1,"return":"geo_code_1,geo_code_2,city,postal_code,state_province_id,street_address","geo_code_1":{"BETWEEN":["-90",90]},"geo_code_2":{"BETWEEN":["-90",90]}}

def getAddressesInSquare(lowerlatitude,upperlatitude,lowerlongitude,upperlongitude):
     #json={"sequential":1,"return":"geo_code_1,geo_code_2","geo_code_1":{"BETWEEN":["lower","upper"]},"geo_code_2":{"BETWEEN":["lower","upper"]}}
     json = {"sequential":1,"options":{"limit":1000},"return":"geo_code_1,geo_code_2,city,postal_code,state_province_id,street_address","geo_code_1":{"BETWEEN":[str(lowerlatitude),str(upperlatitude)]},
          "geo_code_2":{"BETWEEN":[str(lowerlongitude),str(upperlongitude)]}}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Address","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMGETRequest(params=params)
     handeledResponse = responseHandlerBatchValues(response)
     return handeledResponse
     

'''
tests connection to the civicrm server
Returns a boolean
'''
def testconnection(apikey = globalVariables.civiCRMAPIKEY,key=globalVariables.civiCRMKEY):
     param = {"api_key":apikey,"entity":"contact","key":key,"action":"get","json":1,"debug":1,"version":3,"first_name":"Alice","last_name":"Roberts"}
     requested = requests.get(globalVariables.civiCRMURL + globalVariables.civiCRMAPIAddress, json = {"sequential": 1}, params = param)
     helperfunctions.printRequest(requested)
     if(requested.status_code == 200):
          return True
     else:
          return False

#entity=Contact&action=get&api_key=userkey&key=sitekey&json={"sequential":1,"return":"id","email":"","options":{"limit":1}}
def findUserIDByEmail(email):
     json = {"sequential":1,"return":"id","email":str(email),"options":{"limit":1}}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Contact","action":"get",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMGETRequest(params=params)
     return responseHandlerValues(response)["contact_id"]



'''
Creates a User in CiviCRM
The order in which user is created
Create contact in CiviCRM which contains information such as First Name, Last Name, Contact id, Contact Type,
Create email for contact
Create address for contact 
'''

def createUser(contact_id,first_name,last_name,email,canvasser_points=0,canvasser_visits_count=0):
     json={"sequential":1,"contact_type":"Individual","id":contact_id,"first_name":first_name,"last_name":last_name,
          "custom_19":canvasser_points,"custom_20":canvasser_visits_count}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Contact","action":"create",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMPOSTRequest(params=params)
     
     createHandlerError = responseHandlerError(response)
     if(createHandlerError==True):
          updateEmailError = updateUserEmail(contactid=contact_id,email=email)
          return updateEmailError
     else:
          return False

'''
Takes in a user_id and returns a dictionary containing the street_address, city, latitude, longitude, zipcode, state, and id of a given user_id
'''


def userHomeAddress(user_id,apikey=globalVariables.civiCRMAPIKEY,key=globalVariables.civiCRMKEY):
     json = {"id":user_id,"sequential":1,"return":"street_address,city,postal_code,geo_code_1,geo_code_2,state_province"}
     correctlyFormatedJSON = civiCRMJsonCreator(json)
     param = {"api_key":apikey,"key":key,"action":"get","entity":"Contact","json":correctlyFormatedJSON}
     requested = requests.get(globalVariables.civiCRMURL + globalVariables.civiCRMAPIAddress, params= param)
     responseJson = requested.json()
     returnable = {}
     responseJson = responseJson["values"][0]
     returnable["id"] = responseJson["id"]
     returnable["zipcode"] = responseJson["postal_code"]
     returnable["latitude"] = responseJson["geo_code_1"]
     returnable["longitude"] = responseJson["geo_code_2"]
     returnable["city"] = responseJson["city"]
     returnable["street_address"] = responseJson["street_address"]
     return returnable



'''
Asks for a users set of addresses
'''
def userTurf(username,apikey=globalVariables.civiCRMAPIKEY,key=globalVariables.civiCRMKEY):
     param = {"api_key":apikey,"key":key,"entity":"Address","action":"get"}
     requested =  requests.get(globalVariables.civiCRMURL + globalVariables.civiCRMAPIAddress, json = {"sequential":1,"contact_id":"28"}, params = param)
     #helperfunctions.printRequest(requested)

     #http://18.221.10.23:8001/sites/all/modules/civicrm/extern/rest.php?entity=Address&action=get&api_key=userkey&key=sitekey&json={"sequential":1,"contact_id":"user_contact_id"}
     #get addresses from requested


'''
Takes in a streetAddress and returns a list of peopleID's associated with the address
'''

def streetAddressToPeopleID(streetAddress,apikey=globalVariables.civiCRMAPIKEY,key=globalVariables.civiCRMKEY):
     json = {"sequential":1,"return":"id","street_address":"825O El Camino Way NE"}
     jsonString = civiCRMJsonCreator(json)
     param = {"entity":"Contact","action":"get","api_key":apikey,"key":key,"json":jsonString}
     requested = requests.get(globalVariables.civiCRMURL + globalVariables.civiCRMAPIAddress,params=param)
     returnedjson = requested.json()["values"]
     returnable = []
     for i in returnedjson:
          returnable.append(i["id"])
     return returnable

'''
takes in latitude and longitude and returns a dictionary containing the street_address, city, latitude, longitude, zipcode, state, and id of the address

'''

def latlongToAddress(latitude,longitude, apikey=globalVariables.civiCRMAPIKEY,key=globalVariables.civiCRMKEY):
     json={"sequential":1,"geo_code_1":{"LIKE":latitude},"geo_code_2":{"LIKE":longitude}}
     jsonString = civiCRMJsonCreator(json)
     param = {"entity":"Address","action":"get",'api_key':apikey,"key":key,"json":jsonString}
     requested = requests.get(globalVariables.civiCRMURL + globalVariables.civiCRMAPIAddress, params= param)
     returnedjson = requested.json()['values'][0]
     returnable = {}
     returnable["id"] = returnedjson["id"]
     returnable["zipcode"] = returnedjson["postal_code"]
     returnable["latitude"] = returnedjson["geo_code_1"]
     returnable["longitude"] = returnedjson["geo_code_2"]
     returnable["city"] = returnedjson["city"]
     returnable["street_address"] = returnedjson["street_address"]
     return returnable


def createAddress(street_address,city,postal_code,latitude,longitude,country_id="US"):
     json = {'sequential':1,"street_address":str(street_address),"city":str(city),"postal_code":str(postal_code),
          "geo_code_1":str(latitude),"geo_code_2":str(longitude),"country_id":str(country_id)}
     jsonString = civiCRMJsonCreator(json)
     params = {"entity":"Address","action":"create",'api_key':globalVariables.civiCRMAPIKEY,"key":globalVariables.civiCRMKEY,
          "json":jsonString}
     response = createCiviCRMPOSTRequest(params=params)
     print(response.json())
     return responseHandlerError(response)


'''
Take a csv file and write the addresses into civicrm
'''
def addressCSVToCiviCRM(csvfilename):
     pass



def streetAddressToAddressID(streetAddress,apikey=globalVariables.civiCRMAPIKEY,key=globalVariables.civiCRMKEY):
     json={"sequential":1,"street_address":streetAddress}
     jsonString = civiCRMJsonCreator(json)
     param = {"entity":"Address","action":"get","api_key":apikey,"key":key,"json":jsonString}
     requested = requests.get(globalVariables.civiCRMURL + globalVariables.civiCRMAPIAddress, params= param)
     return requested.json()['values'][0]["id"]
