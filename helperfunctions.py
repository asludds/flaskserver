import base64
from flask import request
import zipcode
from globalVariables import globalVariables
import requests

def userHandlerPOSTAttributes(email,password,first_name,last_name,state_code,latitude,longitude,photo_thumb_url="None",photo_large_url="None"):
     return {'email':email,"password":password,"first_name":first_name,"last_name":last_name,
          "state_code":"MA","lat":latitude,"lng":longitude,"photo_thumb_url":photo_thumb_url,"photo_large_url":photo_large_url}

def userHandlerPOSTRelationships():
     return {"visits":{"data":[]},"followers":{"data":[]},"following":{"data":[]}}

def userHandlerPOSTData(userid,attributes,relationships):
     return {"id":userid,"type":'users',"attributes":attributes,"relationships":relationships}

def useridHandlerAttributes(email,password,first_name,last_name,state_code,latitude,longitude,photo_thumb_url,photo_large_url):
     return {'email':email,"password":password,"first_name":first_name,"last_name":last_name,"state_code":state_code,"lat":latitude,"lng":longitude,
          "photo_thumb_url":longitude,"photo_large_url":latitude}

def useridHandlerRelationships(userid,followerid,followingid):
     return {"visits":{"data":[{"id":userid,"type":"visits"}]},"followers":{"data":[{"id":followerid,"type":"users"}]},"following":{"data":[{"id":followingid,"type":"users"}]}}

def addressHandlerGETAttributes(longitude,latitude,street_1,street_2,city,state_code,zip_code,visited_at,best_canvass_response,last_canvass_response):
     return {"longitude":longitude,"latitude":latitude,"street_1":street_1,"street_2":street_2,"city":city,
     "state_code":state_code,"zip_code":zip_code,"visited_at":visited_at,"best_canvass_response":best_canvass_response,"last_canvass_response":last_canvass_response}

def visitHandlerRelationships(userid,addressupdateid,addressid,scoreid):
     user = {"data":{"id":userid,"type":"users"}}
     address_update = {"data":{"id":addressupdateid,"type":"address_updates"}}
     address = {"data":{"id":addressid,"type":"addresses"}}
     person_updates = {"data":[]}
     people = {"data":[]}
     score = {"data":{"id":scoreid,"type":"scores"}}
     relationships = {"user":user,"address_update": address_update,"address":address,"person_updates":person_updates,"people":people,"score":score}
     return relationships

def findStateGivenLatLng(latitude,longitude, findStateRadiusInMiles = 5):
     print(latitude,longitude,"in find state given lat long")
     zipCodeList =  zipcode.isinradius((latitude,longitude) , findStateRadiusInMiles)
     print(zipCodeList[0].state)
     return zipCodeList[0].state

def findStateGivenZipcode(zipcode):
     if(len(zipcode)==4):
          zipcode = "0" + zipcode
     myzip = zipcode.isequal(zipcode)
     return zipcode.state

def findZipcodeGivenLatLng(latitude,longitude,findZipcodeRadiusInMiles = 1.5):
     zipCodeList = zipcode.isinradius((latitude,longitude) , findZipcodeRadiusInMiles)
     return str(zipCodeList[0].zip)

def visitHandlerScoreObject(id,updatePoints,doorKnockPoints,relationshipsID):
     return {"id":id,"type":"scores","attributes":{"points_for_updates":updatePoints,"points_for_knock":doorKnockPoints},"relationships":{"visit":{"data":{"id":relationshipsID,"type":"visits"}}}}

def visitHandlerScore(*args):
     return sum([x for x in args])

def visitHandlerAttributes():
     pass 

def userMePatchHandlerAttributes(email,password,first_name,last_name,state_code,latitude,longitude,photo_thumb_url,photo_large_url):
     return {'email':email,"password":password,"first_name":first_name,"last_name":last_name,"state_code":state_code,"lat":latitude,"lng":longitude,"photo_thumb_url":photo_thumb_url,"photo_large_url":photo_large_url}

def userMePatchHandlerRelationships(userid,attributes,followerid,followingid):
     return {"visits":{"data":[{"id":userid,"type":"visits"}]},"followers":{"data":[{"id":followerid,"type":"users"}]},"following":{"data":[{"id":followingid,"type":"users"}]}}

def rankingHandlerAttributes(email,password,first_name,last_name,state_code,latitude,longitude,photo_thumb_url,photo_large_url):
     return {"email":email,"password":password,"first_name":first_name,"last_name":last_name,"state_code":state_code,"lat":latitude,"lng":longitude,
     "photo_thumb_url":photo_thumb_url,"photo_large_url":photo_large_url}

def printRequest(request):
     try:
          print(request.status_code)
          print("Above is request status code")
     except Exception as e:
          print("Request status code not found exception is ")
          print(e)
     try:
          print(request.body)
          print("Above is request body")
     except Exception as e:
          print("Request body not found exception is ")
          print(e)
     try:
          print(request.form)
          print("Above is request form")
     except Exception as e:
          print("Request form not found exception is")
          print(e)
     try:
          print(request.args)
          print("Above is request args")
     except Exception as e:
          print("Request args not found exception is ")
          print(e)
     try:
          print(request.json())
          print("Above is request json")
     except Exception as e:
          print("Request json not found exception is")
          print(e)
     try:
          print(request.header)
          print("Above is header")
     except Exception as e:
          print("Request header not found exception is ")
          print(e)
     try:
          print(request.text)
          print("Above is text")
     except Exception as e:
          print("Request text not found exception is")
          print(e)
     try:
          print(request.encoding)
          print("Above is request encoding")
     except Exception as e:
          print("Request encoding not found exception is ")
          print(e)


def base64ClientIDClientSecretDecoder(authorization):
     decoded = base64.b64decode(authorization)
     decoded = decoded.decode('utf-8')
     #check if binary string
     if(decoded[0]=='b' and decoded[1]=="'"):
         decoded = decoded[2:-1]
     splitOnCollon = str(decoded).split(":")
     decodedClientID = str(splitOnCollon[0])
     decodedClientSecret = str(splitOnCollon[1])
     return decodedClientID, decodedClientSecret

def fetchWalkListIDFromNationBuilderAddressRequest(request):
     requestJson = request.json()
     try:
          requestJson['message']=="Record not found"
          return None
     except:
          pass 
     results = requestJson["results"]
     listOfIDS = [y['id'] for y in results]
     maxid = max(listOfIDS)
     return results['id'==maxid]['id']

def getNationBuilderAddresses(walklistid , nationBuilderSlug , nationBuilderAccessToken , numberOfPeopleResponseLimit=10):
     url = "https://"+ str(nationBuilderSlug)+".nationbuilder.com/api/v1/lists/" + str(walklistid) + "/people"
     walkListAddressRequest = requests.get(url = url , params = {"limit":numberOfPeopleResponseLimit , "access_token":nationBuilderAccessToken} , headers = {
          "Accept-Encoding":"gzip,deflate",
          "Content-Type": "application/json",
          "Accept" : "application/json",
          "User-Agent" : "runscope/0.1"
     })
     return walkListAddressRequest

def findAddressesFromNationBuilderRequest(request):
     try:
          requestJson= request.json()["results"]
     except:
          print("JSON does not have attribute results")
          return []
     '''
     We need longitude, latitude,street_1,street_2,city,state_code,zip_code,visited_at,best_canvass_response,last_canvass_response
     '''
     listOfAddresses = [{"longitude":x["primary_address"]["lng"],"latitude":x["primary_address"]["lat"],"street_1":x["primary_address"]["address1"],"street_2":x["primary_address"]["address2"],"city":x["primary_address"]["city"],"state_code":x["primary_address"]["state"],"zip_code":x["primary_address"]["zip"],"visited_at":0,"best_canvass_response":"best canvass response nation builder","last_canvass_response":"last canvass response nation builder","id":x["id"]} for x in  requestJson]
     return listOfAddresses


'''
Updates the globalVariables.nationBuilderAccessToken variable. Also returns the access token
'''
def updateNationBuilderAccessToken():
     globalVariables.nationBuilderAccessToken = "ae8b060f8dcfabaec1fca6a08ddbd0c613c1eb52dae0015c00ea975d48005e3b"
     return globalVariables.nationBuilderAccessToken

def nationBuilderAddressRequestHandler(nationBuilderSlug , nationBuilderAccessToken , numberOfWalklistSlugLimit = 10):
     return requests.get(url="http://"+ str(nationBuilderSlug) + ".nationbuilder.com/api/v1/lists" , params = {"limit":numberOfWalklistSlugLimit , "__proto__":"" , "access_token":nationBuilderAccessToken})

def nationBuilderSurveyQuestionHandler(includedList):
     for personOrAddress in includedList:
          if(personOrAddress["type"]=="addresses"):
               #if the addresses id exists then we are updating an addresses
               #if address id does not exist then we are creating a new address
               try:
                    addressid = personOrAddress['id']
                    address = personOrAddress['attributes']
                    latitude = address['latitude']
                    longitude = address['longitude']
                    street_1 = address['street_1']
                    street_2 = address['street_2']
                    city = address['city']
                    state_code = address['state_code']
                    zip_code = findZipcodeGivenLatLng(latitude = latitude , longitude = longitude)
                    try:
                         best_canvass_response = address['best_canvass_response']
                    except:
                         best_canvass_response = ""
                    #update address information


               except:
                    address = personOrAddress['attributes']
                    address = personOrAddress['attributes']
                    latitude = address['latitude']
                    longitude = address['longitude']
                    street_1 = address['street_1']
                    street_2 = address['street_2']
                    city = address['city']
                    state_code = address['state_code']
                    zip_code = findZipcodeGivenLatLng(latitude = latitude, longitude = longitude)
                    try:
                         best_canvass_response = address['best_canvass_response']
                    except:
                         best_canvass_response = ""
                    #create new address
                    print("Nation Builder Address trying to be created")



               #update address based on new information
          elif(personOrAddress["type"]=="people"):
               #check if the person is being updated or new
               try:
                    personid = personOrAddress['id']
                    person = personOrAddress['attributes']
                    first_name = person['first_name']
                    last_name = person['last_name']
                    email = person['email']
                    phone = person['phone']
                    try:
                         preferred_contact_method = person['preferred_contact_method']
                    except:
                         preferred_contact_method = ""
                    try:
                         previous_participated_in_caucus_or_primary = person['previous_participated_in_caucus_or_primary']
                    except:
                         previous_participated_in_caucus_or_primary = ""
                    party_affiliation = person['party_affiliation']
                    canvass_response = person['canvass_response']
                    #person is being updated
                    personJson = {"id":personid,"email":email,"last_name":last_name,"first_name":first_name,"phone":phone,"party":party_affiliation,"is_active_voter":previous_participated_in_caucus_or_primary}
                    nationBuilderUpdatePerson(personJson=personJson)

               except:
                    person = personOrAddress['attributes']
                    first_name = person['first_name']
                    last_name = person['last_name']
                    email = person['email']
                    phone = person['phone']
                    try:
                         preferred_contact_method = person['preferred_contact_method']
                    except:
                         preferred_contact_method = ""
                    try:
                         previous_participated_in_caucus_or_primary = person['previous_participated_in_caucus_or_primary']
                    except:
                         previous_participated_in_caucus_or_primary = ""
                    party_affiliation = person['party_affiliation']
                    canvass_response = person['canvass_response']
                    #person is being added
                    personJson = {"email":email,"last_name":last_name,"first_name":first_name,"phone":phone,"party":party_affiliation,"is_active_voter":previous_participated_in_caucus_or_primary}
                    nationBuilderCreatePerson(personJson=personJson)
          else:
               pass

def nationBuilderCreatePerson(personJson,nationBuilderSlug=globalVariables.nationBuilderSlug,nationBuilderAccessToken=globalVariables.nationBuilderAccessToken):
    url = "https://"+ str(nationBuilderSlug)+".nationbuilder.com/api/v1/people"
    personJson['access_token'] = str(nationBuilderAccessToken)
    createPersonResponse = requests.post(url = url , params = personJson , headers = {
          "Accept-Encoding":"gzip,deflate",
          "Content-Type": "application/json",
          "Accept" : "application/json",
          "User-Agent" : "runscope/0.1"
     })
    return createPersonResponse

def nationBuilderUpdatePerson(personJson , nationBuilderSlug=globalVariables.nationBuilderSlug , nationBuilderAccessToken = globalVariables.nationBuilderAccessToken):
     id = personJson['id']
     url = "https://"+ str(nationBuilderSlug)+".nationbuilder.com/api/v1/people/" + str(id)
     personJson['access_token'] = str(nationBuilderAccessToken)
     updatePersonReseponse = requests.post(url = url, params = personJson , headers = {
           "Accept-Encoding":"gzip,deflate",
          "Content-Type": "application/json",
          "Accept" : "application/json",
          "User-Agent" : "runscope/0.1"
     })
     return updatePersonReseponse

def nationBuilderCreateAddress():
     pass

def nationBuilderUpdateAddress():
     pass

if __name__=="__main__":
     pass
