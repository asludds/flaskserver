import sys, os, time
sys.path.append("../")
from flask import Flask, url_for, session, request, jsonify, g
from flask_sqlalchemy import SQLAlchemy
import sqlalchemy
from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Float, Boolean, desc
from sqlalchemy.orm import sessionmaker, relationship
import geopy.distance as distance
import base64
import random
from werkzeug.security import generate_password_hash, check_password_hash
import pprint
from sqlalchemy.ext.declarative import declarative_base
from civicrm import *
from globalVariables import *
from helperfunctions import *



if __name__ == "__main__":
    if(globalVariables.serverType=="civicrm"):
        from civicrm.civicrm import civiCRMMain
        civiCRMMain()
    elif(globalVariables.serverType=="nationbuilder"):
        from nationbuilder.nationbuilder import nationBuilderMain
        nationBuilderMain()
    elif(globalVariables.serverType=="ngpvan"):
        from ngpvan.ngpvan import ngpvanMain
        ngpvanMain()
    else:
        print("Server type specified in globalVariables not recognized")
