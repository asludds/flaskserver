import sys
sys.path.append('../')
import flaskserver.main as main
import flaskserver.civicrm as civicrm
from flaskserver.globalVariables import *
import flaskserver.helperfunctions as helperfunctions
from flaskserver.nationbuilder import *
from flaskserver.ngpvan import *

class TestGlobalVariables():
    def test_CLIENT_ID(self):
        assert(type(globalVariables.CLIENT_ID)==str)
        assert(globalVariables.CLIENT_ID != "")

    def test_CLIENT_SECRET(self):
        assert(type(globalVariables.CLIENT_SECRET)==str)
        assert(type(globalVariables.CLIENT_ID)==str)

    def test_serverType(self):
        serverType = globalVariables.serverType
        assert(serverType=="civicrm" or serverType=="nationbuilder" or serverType == "ngpvan")

    def test_nationBuilderSlug(self):
        assert(type(globalVariables.nationBuilderSlug)==str)
        assert(globalVariables.nationBuilderSlug != "")

    def test_nationBuilderAPIKey(self):
        pass

    def test_nationBuilderURL(self):
        nationBuilderURL = globalVariables.nationBuilderURL
        assert(type(nationBuilderURL)==str)

    def test_updatePoints(self):
        updatePoints = globalVariables.updatePoints
        assert(type(updatePoints)==int)
        assert(updatePoints >= 0)

    def test_doorKnockPoints(self):
        pass

    def test_environment(self):
        assert(type(globalVariables.environment) == str)
        assert(globalVariables.environment == "testing" or globalVariables.environment == "production")

    def test_civiCRMIDUpperBound(self):
        pass

    def test_upperBoundRandRange(self):
        pass

    def test_oauthTimeLimitSeconds(self):
        pass

    def test_numberOfProcesses(self):
        pass

    def test_databaseURI(self):
        pass

    def test_SQLALCHEMY_TRACK_MODIFICATIONS(self):
        pass

    def test_debugBoolean(self):
        pass

    def test_OAUTHLIB_INSECURE_TRANSPORT(self):
        pass

    def test_OS_DEBUG(self):
        pass

    def test_civiCRMAPIKEY(self):
        pass

    def test_civiCRMKEY(self):
        pass

    def test_civiCRMURL(self):
        pass

    def test_civiCRMAPIAddress(self):
        pass

    def test_compatibleVersions(self):
        pass

    def test_serverurl(self):
        pass

class TestCiviCRM():
    def test_ping(self):
        pass

class TestNationBuilder():
    def test_oauthtokenPOSTNationBuilder(self):
        pass

    def test_usersPOSTHandlerNationBuilder(self):
        pass

    def test_userMePatchNationBuilder(self):
        pass

    def test_usersIDNationBuilder(self):
        pass

    def test_usersMeGETNationBuilder(self):
        pass

    def test_addressesGETNationBuilder(self):
        pass

    def test_visitsPOSTNationBuilder(self):
        pass

    def test_rankingsGETNationBuilder(self):
        pass

    def test_devicesGETNationBuilder(self):
        pass

    def test_pingGETNationBuilder(self):
        pass

    def test_oauthverificationNationBuilder(self):
        pass

    def test_before_request(self):
        pass

class TestNPGVAN():
    def test_oauthtokenPOSTNationBuilder(self):
        pass

    def test_usersPOSTHandlerNationBuilder(self):
        pass

    def test_userMePatchNationBuilder(self):
        pass

    def test_usersIDNationBuilder(self):
        pass

    def test_usersMeGETNationBuilder(self):
        pass

    def test_addressesGETNationBuilder(self):
        pass

    def test_visitsPOSTNationBuilder(self):
        pass

    def test_rankingsGETNationBuilder(self):
        pass

    def test_devicesGETNationBuilder(self):
        pass

    def test_pingGETNationBuilder(self):
        pass

    def test_oauthverificationNationBuilder(self):
        pass

    def test_before_request(self):
        pass


class Testhelperfunctions():
    def test_updateNationBuilderAccessToken(self):
        # globalVariablesClass = globalVariables
        # globalVariablesClass.nationBuilderAccessToken = ""
        # returnedUpdate = helperfunctions.updateNationBuilderAccessToken()
        # assert(globalVariablesClass.nationBuilderAccessToken != "")
        # assert(returnedUpdate != "")
        pass

    def test_getNationBuilderAddresses(self):
       accessToken = helperfunctions.updateNationBuilderAccessToken() 
       testingWalkListID = 2
       testReturn = helperfunctions.getNationBuilderAddresses(walklistid=testingWalkListID , nationBuilderSlug = globalVariables.nationBuilderSlug , nationBuilderAccessToken = accessToken)
       assert(testReturn.status_code == 200)
       assert(testReturn.json())
       assert(testReturn.json()['results'] != None)
       assert(testReturn.json()['results'] != [])

    def test_findAddressFromNationBuilderRequest(self):
        accessToken = helperfunctions.updateNationBuilderAccessToken()
        request = helperfunctions.getNationBuilderAddresses(walklistid=2,nationBuilderSlug = globalVariables.nationBuilderSlug , nationBuilderAccessToken = accessToken)
        testReturn = helperfunctions.findAddressesFromNationBuilderRequest(request=request)
        assert(testReturn!=[])
        assert(testReturn != None)

    def test_findZipcodeGivenLatLngCambridge(self):
        #test cambridge MA zipcode
        cambridgeLatitude = 42.361873
        cambridgeLongitude = -71.095430
        cambridgeZipCode = "02138"
        testZipcode = helperfunctions.findZipcodeGivenLatLng(cambridgeLatitude , cambridgeLongitude)
        assert(cambridgeZipCode==testZipcode)
 
    def test_findZipcodeGivenLatLngSanFrancisco(self):
        #test downtown san francisco painted ladies
        sanfranciscoLatitude = 37.777602
        sanfranciscoLongitude = -122.433589
        sanfranciscoZipcode = "94115"
        testZipcodeSanfrancisco = helperfunctions.findZipcodeGivenLatLng(sanfranciscoLatitude , sanfranciscoLongitude)
        assert(testZipcodeSanfrancisco == sanfranciscoZipcode)




