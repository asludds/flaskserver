from globalVariables import *
from threading import Thread
import base64
import requests
from helperfunctions import printRequest
import grequests



'''
Sends a request for addresses with a large area
This function is asyncronous, meaning that we should be able to call it multiple times simultaneously to see how the server handles loads
'''
def login():
     base64encodedIDandSecret =  base64.b64encode((str(globalVariables.CLIENT_ID) +":"+ str(globalVariables.CLIENT_SECRET)).encode("ascii"))
     base64encodedIDandSecret = base64encodedIDandSecret.decode("ascii")
     headers = {"authorization":"Basic " + str(base64encodedIDandSecret),
     "Content-Type": "application/x-www-form-urlencoded; charset=utf-8", "Accept": "application/json"}
     body = {"grant_type":"password","username":"alexsludds@gmail.com","password":"pass"}
     loginRequest = requests.post(globalVariables.serverurl + "/oauth/token",data=body,headers=headers)
     bearerToken = loginRequest.json()['access_token']
     return bearerToken




'''
This function makes many async requests to the server to see how it handles it.
'''

def addressInRadius(latitude,longitude,radius,bearerToken):
     header = {"authorization":"Bearer " + str(bearerToken)}
     params = {"latitude":latitude,"longitude":longitude,"radius":radius}
     addressRequest = requests.get(globalVariables.serverurl + "/addresses",params=params,headers=header)
     addresses = addressRequest.json()
     print(addresses)

def stresstestAddressEndpoint(latitude,longitude,radius,bearerToken,amountOfRequests):
     header = {"authorization":"Bearer " + str(bearerToken)}
     params = {"latitude":latitude,"longitude":longitude,"radius":radius}
     urls = amountOfRequests*[globalVariables.serverurl+"/addresses"]
     print(urls)
     rs = (grequests.get(u,params=params,headers=header) for u in urls)
     mapped = grequests.map(rs)
     print(mapped)


if __name__=="__main__":
     bearerToken = login()
     #addressInRadius(latitude=42.368120,longitude=-71.097651,radius=1000,bearerToken=bearerToken)
     stresstestAddressEndpoint(amountOfRequests=30,latitude=42.368120,longitude=-71.097651,radius=1000,bearerToken=bearerToken)
